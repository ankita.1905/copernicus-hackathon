# VelAir : Breathe Easy ... Breathe Healthy ...

## Project built for Copernicus Hackathon Amersfoort, 2020: Case Velsen.

### Steps to set up the project in local development environement

#### Pre-requisites: Minimum requirements are installed npm or yarn package manager and ofcourse access to internet.

#### Step 1: Clone the repository and run `npm install` or `yarn` in both the app and api directories of the project.

#### Step 2: From the /api directory run `yarn watch` or `npm run watch`. Doing this starts the backend API service.

#### Step 3: From the /app directory run `yarn watch` or `npm run watch`. Doing this starts the frontend APP service.

#### Step 4: Go to localhost://5000 and you should see the application rendered there.
