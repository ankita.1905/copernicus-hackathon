var fs = require("fs-extra");
var readline = require("readline");
var path = require("path");

const loadFile = (filename) => {
  var filePath = "./data/" + filename + ".txt";

  const filedata = [];
  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(filePath);
    const rl = readline.createInterface({ input: stream, crlfDelay: Infinity });

    rl.on("line", (l) => {
      var k = l.split(",");
      var final = [];
      final.push(parseFloat(k[0]));
      final.push(parseFloat(k[1]));
      final.push(k[2]);

      filedata.push(final);
    });

    rl.on("close", () => resolve(filedata));
  });
  return filedata;
};

module.exports = loadFile;
