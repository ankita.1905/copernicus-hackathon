var loadFile = require("./../services/service");
var express = require("express");
var router = express.Router();

router.get("/", async (req, res) => {
  console.log(req.query.datafile);
  var testing = `${req.query.datafile}`;
  console.log(testing);
  var testdata = loadFile(testing);
  testdata.then(function (result) {
    res.send(result);
  });
});

module.exports = router;
