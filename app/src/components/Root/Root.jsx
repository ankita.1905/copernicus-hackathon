import * as React from "react";
import styled from "styled-components";

import MapPage from "./MapPage";

const Wrapper = styled.div``;

const Root = () => {
  return (
    <Wrapper>
      <MapPage />
    </Wrapper>
  );
};

export default Root;
